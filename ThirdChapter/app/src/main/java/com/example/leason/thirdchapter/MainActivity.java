package com.example.leason.thirdchapter;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {
    private EditText editText;
    ImageView imageView;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button=(Button) findViewById(R.id.button);
        editText=(EditText) findViewById(R.id.edit_text);
        imageView=(ImageView) findViewById(R.id.image_view);
        progressBar=(ProgressBar) findViewById(R.id.progress_bar); //进度条处理90
        //使用匿名类方法来注册监听器
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //在此处添加逻辑
                switch (v.getId()) {
                    case R.id.button:
//                    imageView.setImageResource(R.drawable.img2);//动态的更改ImageView中的图片87页

//                        if(progressBar.getVisibility()==View.GONE){
//                            progressBar.setVisibility(View.GONE);
//                        }
//                        else {
//                            progressBar.setVisibility(View.GONE);
//                        }

//                        int progress=progressBar.getProgress();
//                        progress=progress+10;
//                        progressBar.setProgress(progress);
//                        //91页
                        //AlertDiaog
                        AlertDialog.Builder dialog=new AlertDialog.Builder(MainActivity.this);
                        dialog.setTitle("这是一个Dialog");
                        dialog.setMessage("有一些比较重要的");
                        dialog.setCancelable(false);
                        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        dialog.show();
                    break;
                    default:
                        break;
                }
            }
        });
    }
}

package com.example.leason.secondexperiment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button bnStandard = (Button) findViewById(R.id.buttonStandard);
        Button bnSingleTop = (Button) findViewById(R.id.buttonSingleTop);
        Button bnSingleTask = (Button) findViewById(R.id.buttonSingleTask);
        Button bnSingleInstance = (Button) findViewById(R.id.buttonSingleInstance);

        bnStandard.setOnClickListener(this);
        bnSingleTop.setOnClickListener(this);
        bnSingleTask.setOnClickListener(this);
        bnSingleInstance.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonStandard:
                Intent itStandard = new Intent(this,Standard.class);
                startActivity(itStandard);
                break;
            case R.id.buttonSingleTop:
                Intent itSingleTop = new Intent(this,SingleTop.class);
                startActivity(itSingleTop);
                break;
            case R.id.buttonSingleTask:
                Intent itSingleTask = new Intent(this,SingleTask.class);
                startActivity(itSingleTask);
                break;
            case R.id.buttonSingleInstance:
                Intent itSingleInstance = new Intent(this,SingleInstance.class);
                startActivity(itSingleInstance);
                break;
            default:break;
        }
    }
}

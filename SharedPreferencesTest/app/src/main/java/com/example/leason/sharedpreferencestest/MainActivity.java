package com.example.leason.sharedpreferencestest;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Button saveData = (Button) findViewById(R.id.save_data);
//        saveData.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences.Editor editor = getSharedPreferences("data", MODE_PRIVATE).edit();
//                editor.putString("name", "Tom");
//                editor.putInt("age", 28);
//                editor.putBoolean("married", false);
//                editor.apply();
//            }
//        });
        Button restroreData=(Button) findViewById(R.id.restore_data);
        restroreData.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                SharedPreferences pre=getSharedPreferences("data",MODE_PRIVATE);
                String name=pre.getString("name","");
                int age=pre.getInt("age",0);
                boolean married=pre.getBoolean("married",false);
                Log.d("MainActivity","name id "+name);
                Log.d("MainActivity","age is "+age);
                Log.d("MainActivity","married id "+married);
            }
        });
    }
}
package com.example.leason.fragmentbestpractice;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class NewsContentActivity extends AppCompatActivity {
    public static void actionStart(Context context,String newsTitle,String newsContent){
        Intent intent=new Intent(context,NewsContentActivity.class);
        intent.putExtra("new_title",newsTitle);
        intent.putExtra("news_content",newsContent);
        context.startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_content);
        String newTitle=getIntent().getStringExtra("new_title");
        String newContent=getIntent().getStringExtra("new_content");
       NewContentFragment newContentFragment=(NewContentFragment)getSupportFragmentManager().findFragmentById(R.id.new_content_fragment);
       newContentFragment.refresh(newTitle,newContent);
    }

}
